var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment');

const MongoClient = require("mongodb").MongoClient;
const consumption = "Consumption";
const generation = "EnergyGenerated";

const dbConnectionUrl = "mongodb+srv://ak125:ecowatt2020@cluster0-cjt2c.mongodb.net/test?retryWrites=true&w=majority";

const db = require("../db");
const dbName = "EcoWatt";
const deviceCollection = "Devices";
const roomCollection = "Rooms";
const scanDevicesCollection = "ScanDevices";

router.get('/new', function(req, res, next) {
  let roomID = req.query.roomID;
  db.initialize(dbName, roomCollection, function(dbCollection) {
    dbCollection.findOne({ _id : new ObjectId(roomID)}, function(error, result) {
    if (error) throw error;
    res.render('devices/add',{
      room : result,
    });
  })
  }, function(err) {
    throw (err);
  });
});


router.post('/scanqr', function(req, res, next) {
 
  var info= req.body.text;
  console.log(info);
  var final  = JSON.parse(info);


    var device = {
        deviceName: final.deviceName,
        deviceCategory: final.deviceCategory,
        status: "OFF",
        roomNo: new ObjectId(req.body.roomNo),
        deviceIcon : final.deviceIcon,
        houseID : res.locals.activeHome._id,
        user : " "
        
      }

   if(final.deviceCategory == "lights"){

     device.brightness = 50;
     device.color = "rgb(255, 255, 0)";
     device.deviceIcon = "eco-029-ceiling-light";

   }
   else if(final.deviceCategory == "thermostats"){
    device.deviceIcon="eco-001-air-conditioner";
    device.temp = 20;
    device.mode = "fan";
   }
   else if(final.deviceCategory == "tvs"){
    device.deviceIcon="eco-046-television";
    device.channel = 10;
     
   }
   else if(final.deviceCategory == "ovens"){
    device.deviceIcon ="eco-031-microwave";   
    device.temp = 100;
    device.mode = "heat";  
   }
   else if(final.deviceCategory == "speakers"){
    device.deviceIcon="flaticon-radio"; 
    device.mode = "play";
    device.volume = 15;   
   }
   else if(final.deviceCategory == "fans"){
     device.deviceIcon="flaticon-fan"; 
     device.speed = 2;
   }
   else if(final.deviceCategory == "washing"){
    device.deviceIcon="flaticon-washing-machine"; 
    device.speed=10; 
    device.mode= "dry";     
   }
   else if(final.deviceCategory == "vacuums"){
    device.deviceIcon ="flaticon-vacuum-cleaner"; 
    device.mode = "high";   
    device.speed = 50;
   }
   else if(final.deviceCategory == "refrigerators"){
    device.deviceIcon="eco-021-fridge";     
    device.mode="freez"; 
    device.temp = 4;  
   }
   

   console.log(device);

   
    
      db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback
     
      dbCollection.insertOne(device, function(error, result) { // callback of insertOne
          if (error) throw error;

          db.initialize(dbName, scanDevicesCollection, function(dbCollection) { // successCallback
     
            dbCollection.deleteOne({ "deviceName" : device.deviceName }, function(error, result) { // callback of insertOne
                if (error) throw error;
                
            });
           
            }, function(err) { // failureCallback
              throw (err);
            });
      
          res.json({msg: "done!"});
          
      });
     
      }, function(err) { // failureCallback
        throw (err);
      });


});
 
router.get('/scan', function(req, res, next) {

  db.initialize(dbName, scanDevicesCollection, function(dbCollection) { // successCallback
    var ID = req.query.id;
    var roomName = req.query.room;

    dbCollection.find().toArray(function(error, result) {
      if (error) throw error;

      res.render('devices/scan', {
        devices: result,
        room: roomName,
        title: 'Device-Loader',
      });

    }, function(err) { // failureCallback
    throw (err);
    });

});

});

router.post('/addDev', function(req, res, next) {
  var final= req.body;

    var device = {
        deviceName: final.deviceName,
        deviceCategory: final.deviceCategory,
        status: "OFF",
        roomNo: new ObjectId(req.body.roomNo),
        deviceIcon: final.deviceIcon,
        houseID : res.locals.activeHome._id,
        user : " "
        
      }

   if(final.deviceCategory == "lights"){

     device.brightness = 50;
     device.color = "rgb(255, 255, 0)";

   }
   else if(final.deviceCategory == "thermostats"){
    device.deviceIcon="eco-001-air-conditioner";
    device.temp = 20;
    device.mode = "fan";
   }
   else if(final.deviceCategory == "tvs"){
    device.deviceIcon="eco-046-television";
    device.mode = "AV";
    device.volume = 15;
     
   }
   else if(final.deviceCategory == "ovens"){
    device.deviceIcon ="eco-031-microwave";   
    device.temp = 100;
    device.mode = "heat";  
   }
   else if(final.deviceCategory == "speakers"){
    device.deviceIcon="flaticon-radio"; 
    device.volume = 15;   
   }
   else if(final.deviceCategory == "fans"){
     device.deviceIcon="eco-019-fan"; 
     device.speed = 2;
   }
   else if(final.deviceCategory == "washing"){
    device.deviceIcon="flaticon-washing-machine"; 
    device.speed=10; 
    device.mode= "delicate";     
   }
   else if(final.deviceCategory == "vacuums"){
    device.deviceIcon ="flaticon-vacuum-cleaner"; 
    device.mode = "high";    
   }
   else if(final.deviceCategory == "refrigerators"){
    device.deviceIcon="eco-021-fridge";     
    device.mode="freez"; 
    device.temp = 4;  
   }
   

   console.log(device);
    


      db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback
     
      dbCollection.insertOne(device, function(error, result) { // callback of insertOne
          if (error) throw error;

          db.initialize(dbName, scanDevicesCollection, function(dbCollection) { // successCallback
     
            dbCollection.deleteOne({ "deviceName" : device.deviceName }, function(error, result) { // callback of insertOne
                if (error) throw error;
                
            });
           
            }, function(err) { // failureCallback
              throw (err);
            });

          res.json({msg: "done!"});
          
      });
     
      }, function(err) { // failureCallback
        throw (err);
      });

  

});


/* GET device details - read /:id */ 
router.get("/:devID", function(req, res){

  let devID = req.params.devID;

  db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback
     
    dbCollection.findOne({ _id : new ObjectId(devID)}, function(error, result) {
      if (error) throw error;

      dbCollection.find().limit(5).toArray(function(error, _result_) {
        if (error) throw error;

        result.devices = _result_;

        db.initialize(dbName, roomCollection, function(dbCollection) { // successCallback

         const roomID= result.roomNo;
        //  console.log("Room with id: ", roomID);
    
           dbCollection.findOne({ _id : roomID}, function(_error, _result) {
            if (_error) throw _error;
            result.room = _result;

            var page;
            if(result.deviceCategory == "lights"){
              page = "Lights";
            }
            else if(result.deviceCategory == "thermostats"){
              page = "Ac";
            }
            else if(result.deviceCategory == "ovens"){
              page = "oven";
            }
            else if(result.deviceCategory == "tvs"){
              page = "TV";
            }
            else if(result.deviceCategory == "fans"){
              page = "Fans";
            }
            else if(result.deviceCategory == "refrigerators"){
              page = "Refrigerator";
            }
            else if(result.deviceCategory == "speakers"){
              page = "Speaker";
            }
            else if(result.deviceCategory == "washing"){
              page = "Washing";
            }
            else if(result.deviceCategory == "vacuums"){
              page = "Vacuum";
            }
            
            res.render(`devices/${page}`, {
              result: result,
            });

        });
        }, function(err) { // failureCallback
          throw (err);
        });     

  });
  }); 

});

  }, function(err) { // failureCallback
    throw (err);
  });
  

/* GET edit form - edit */
router.get('/:id/edit', function(req, res, next) {

  db.initialize(dbName, roomCollection, function(dbCollection) { // successCallback
    var ID = req.params.id;
   // var ID= 'Kitchen';
   
   dbCollection.findOne({ _id : new ObjectId(ID)}, function(error, result) {
     if (error) throw error;
    //  console.log(result);
    
    res.render('rooms/edit', {
      title: 'Rooms - Edit',
      result: result
    });

 });
 }); 
  
});

/* POST route to delete device */
router.post('/:id/delete', function(req, res, next) {
  // Get the device id
  const deviceID=req.params.id;

  // Update the db to delete the device
  db.initialize(dbName, deviceCollection, function(dbCollection) {
    dbCollection.deleteOne({ _id : new ObjectId(deviceID)}, function(error, result) {
    if (error) throw error;
    res.json({msg: "Done!"});
  });
  }, function(err) {
    throw (err);
  });
});

router.post('/changeStatus', function(req, res, next) {

  // Get the device information
  let {deviceID, status} = req.body;
  console.log(deviceID, status, "Incoming Request");

  // Update the device status in database
  db.initialize(dbName, deviceCollection, function(dbCollection) {
    dbCollection.findOneAndUpdate({
      _id : new ObjectId(deviceID)
    },{
      $set : {
        status : status,
        user : res.locals.activeUser._id
       }
    },{
      returnOriginal : false,
    },function(error, updatedDevice) {
      if (error) throw error;
      console.log("Device Status Updated!");
      // If the device is ON
      if(status == 'ON'){
        // Get updated device information
        let {_id, deviceName, roomNo} = updatedDevice.value;

        // Get the generated energy
        var generatedEnergy = Math.floor(Math.random() * (20 - 5 + 1)) + 5;
        
        // Get the consumption data
        var d = new Date();
        let consumptionData = {
          time: moment(d).format('HH:mm'),
          day: moment(d).format('d'),
          date: moment(d).format('ll'),
          energy: generatedEnergy,
          deviceName: deviceName,
          user : res.locals.activeUser._id,
          homeID : res.locals.activeHome._id,
          deviceID: _id,
          roomID: roomNo,
        }
 
        // Save the consumption data in the database
        db.initialize(dbName, consumption, function(dbCollection) { 
          dbCollection.insertOne(consumptionData, 
            function(error, result) {
            if (error) throw error;
            console.log("Device Consumption Added!");
           
          });
        });
      }
      return res.json({msg:"Device Status Updated!"});
    })
  });

 

});

/* POST edit room /:id */
router.post('/:id/changeColor', function(req, res, next) {

  const itemID = req.query.id;
  console.log(itemID);
  const item = req.body;
  
  console.log("Editing item: ", item);

db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback
var ID = req.params.id;

dbCollection.updateOne({ _id : new ObjectId(itemID) },{ $set: { color: item.color} }, function (error, result) {
if (error) throw error;

console.log("updated!")
res.json({msg: "Color changed"} );

}, function(err) { // failureCallback
throw (err);
});

}); 

});

/* POST edit room /:id */
router.post('/:id/changeBrightness', function(req, res, next) {

  const itemID = req.body._id;
  // console.log(req.body);
  const item = req.body;
  var number = parseInt(item.brightness);

db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback
var ID = req.params.id;

dbCollection.updateOne({ _id : new ObjectId(itemID) },{ $set: {brightness: number} }, function (error, result) {
if (error) throw error;

console.log("updated!")
res.json({msg: "Brightness changed"} );

}, function(err) { // failureCallback
throw (err);
});

}); 

});

router.post('/:id/changeTemp', function(req, res, next) {

  const itemID = req.body.deviceID;

db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback
var ID = req.params.id;

dbCollection.updateOne({ _id : new ObjectId(itemID) },{ $set: {temp: req.body.temp} }, function (error, result) {
if (error) throw error;

 
res.json({msg:"temprature changed"})

}, function(err) { // failureCallback
throw (err);
});

}); 

});


router.post('/:id/changeSpeed', function(req, res, next) {

  const itemID = req.body.deviceID;

db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback
var ID = req.params.id;

dbCollection.updateOne({ _id : new ObjectId(itemID) },{ $set: {speed: req.body.speed} }, function (error, result) {
if (error) throw error;

 
res.json({msg:"speed changed"})

}, function(err) { // failureCallback
throw (err);
});

}); 

});

router.post('/:id/changeVol', function(req, res, next) {

  const itemID = req.body.deviceID;

db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback
var ID = req.params.id;

dbCollection.updateOne({ _id : new ObjectId(itemID) },{ $set: {volume: req.body.volume} }, function (error, result) {
if (error) throw error;

 
res.json({msg:"vol changed"})

}, function(err) { // failureCallback
throw (err);
});

}); 

});

router.post('/:id/changeMode', function(req, res, next) {

  const itemID = req.body.deviceID;

db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback
var ID = req.params.id;

dbCollection.updateOne({ _id : new ObjectId(itemID) },{ $set: {mode: req.body.mode} }, function (error, result) {
if (error) throw error;

res.json({msg:"mode changed"})

}, function(err) { // failureCallback
throw (err);
});

}); 

});

/* getDemoLEDs
 * get devices of category 'demo-leds'
 * 
 * USAGE:
getDemoLEDs(function(devices) {
  console.log(devices);
  // DO STUFF HERE  
});
*/
function getDemoLEDs(callback) {
  db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback  
    dbCollection.find({ deviceCategory: 'demo-led'}).toArray(function(error, result) {
      if (error) throw error;
      return callback(result);
    });
  });
}



  




module.exports = router;