var express = require('express');
var router = express.Router();
var moment = require('moment');


/**
 *************************************************************************
 * Routines
 *************************************************************************
 */

/**
 * Routines
 * GET route to display the routines
 */
const MongoClient = require("mongodb").MongoClient;
const ObjectId = require('mongodb').ObjectID;
const dbConnectionUrl = "mongodb+srv://ak125:ecowatt2020@cluster0-cjt2c.mongodb.net/test?retryWrites=true&w=majority";
const _ = require('lodash');
const userMiddleware = require('../middleware/user');

const db = require("../db");
const dbName = "EcoWatt";
const devicesCollection = "Devices";
const roomCollection = "Rooms";
const userCollection = "Users";
const homeCollection = "Homes";
const routineCollection = "Routines";
const netCollection = "NeuralNetwork";

const NeuralNetwork = require('../public/net/net');
const Matrix = require('../public/net/matrix');

const schedule = require('node-schedule');

const weekday = ["SUN","MON","TUE","WED","THU","FRI","SAT"]; // Used to get current day of the week

/* GET routines listing - browse */
router.get('/', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('readAny', 'routine'), function(req, res, next) {

  db.initialize(dbName, routineCollection, function(dbCollection) { // successCallback

        dbCollection.find({homeID : req.cookies.home._id}).toArray(function(error, result) {
            if (error) throw error;

            console.log(JSON.stringify(result));
                res.render('routines/browse', {
                  routines : result,
                });
        });

  }, function(err) { // failureCallback
    throw (err);
  });

});

router.get('/probs', function(req, res, next) {

  getDailyProbabilities(function(consumptionMatrix, timeArray){
  
    console.log("HEYLPPPPP")
    // console.log(consumptionMatrix)
    // console.log(timeArray)

    MongoClient.connect(dbConnectionUrl,{ useUnifiedTopology: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db("EcoWatt");
      var devices;
      dbo.collection('NeuralNetwork').findOne({_id : 1}, function(err, result) {
        if (err) throw err;
        devices = result.devices;
        var dataset = [];

      // console.log(devices)

      for (let i = 0; i < devices.length; i++) {    

        const device = devices[i];
        if(!device.defunct){

          var item = {};
          item.seriesname = device.deviceName;
          itemanchorsides  = 3,
          item.color = "#" + Math.floor(Math.random()*16777215).toString(16);
          item.anchorradius = 3;
          item.anchorbgcolor = item.color;
          item.anchorbordercolor = item.color;

          item.data = [];
          for (let j = 0; j < timeArray.length; j++) {
        
            var obj = {};
            obj.id = item.seriesname;
            obj.x = timeArray[j];
            obj.y = consumptionMatrix[j][i]*100;

            item.data.push(obj);
            
          }
            dataset.push(item);

        }
        
      }

      let category = [];
      for (let j = 0; j < timeArray.length; j++) {
        
        var obj = {};
        obj.label = timeArray[j];
        obj.x = timeArray[j];
        // obj.showverticalline = "1";

        category.push(obj);
        
      }

      // console.log(dataset)
      // console.log(category)

      let returnResult = {};
      returnResult.dataset = dataset;
      returnResult.category = category;
    
      res.status(200).json(returnResult)

   });
  });

  
  });
  
  });


router.get('/probsRoom', function(req, res, next) {

  getDailyProbabilities(function(consumptionMatrix, timeArray){
  
    console.log("HEYLPPPPP")
    MongoClient.connect(dbConnectionUrl,{ useUnifiedTopology: true }, function(err, db) {
      if (err) throw err;
      var dbo = db.db("EcoWatt");
      var devices;
      let step1=  new Promise(function(resolve, reject){
        dbo.collection('Rooms').aggregate([
          { $lookup:
             {
               from: 'Devices',
               localField: '_id',
               foreignField: 'roomNo',
               as: 'roomDevices'
             }
           },{
            $match :{
              homeID : req.cookies.home._id
            }
          }
          ]).toArray(function(error, result) {
            error ? reject(error) : resolve(result);

        });
      });

  step1.then( function(result){

    let rooms = result;

      dbo.collection('NeuralNetwork').findOne({_id : 1}, async function(err, _res) {
        if (err) throw err;
        devices = _res.devices;
        // console.log("devs")
        // console.log(devices)
        var dataset = [];    
        var roomData = []; 

          for (let q = 0; q < rooms.length; q++) {
            var room = rooms[q];
            var devs = [];
            var item = {};
            item.seriesname = room.roomName;
            itemanchorsides  = 3,
            item.color = "#" + Math.floor(Math.random()*16777215).toString(16);
            item.anchorradius = 3;
            item.anchorbgcolor = item.color;
            item.anchorbordercolor = item.color; 
            item.data=[];
            room.roomSum = 0;
            room.roomMaxTime;
            
            var devs = await getStuff(room, devices);
           
            console.log("lolololol")
            // console.log(devs)

            for (let j = 0; j < timeArray.length; j++) {
        
              var obj = {};
              obj.id = item.seriesname;
              obj.x = timeArray[j];
              obj.y = 0;
              
           devs.forEach(el => {
            //  console.log(el, " ell")
            obj.y += consumptionMatrix[j][el]*100;    

           });
         
           obj.y = obj.y/room.roomDevices.length; 
           
           if(obj.y > room.roomSum){
            room.roomSum = obj.y;
            room.roomMaxTime = timeArray[j];
          } 

           item.data.push(obj);
              
            }

            dataset.push(item);
            roomData.push(room);
            
          }

      let category = [];
      for (let j = 0; j < timeArray.length; j++) {
        
        var obj = {};
        obj.label = timeArray[j] + " time";
        obj.x = timeArray[j];
        category.push(obj);
        
      }

      // console.log(dataset)
      // console.log(category)

      let returnResult = {};
      returnResult.dataset = dataset;
      returnResult.category = category;
      returnResult.roomData = roomData;
      console.log("{OPIHLJNMHBJB")
      console.log(roomData)
    
      res.status(200).json(returnResult)

   });
  });
    });
  
  });
  
  });

  async function getStuff(room, devices){

    var devs = [];
  
    for(let p=0; p< room.roomDevices.length; p++){        
      for(let k=0; k<devices.length; k++){
        if(room.roomDevices[p].deviceName == devices[k].deviceName){
    // console.log("wkjbfkhefhgkunhlifoiwuo")
        //  console.log(devices[k].deviceName)
        //  console.log(room.roomDevices[p].deviceName)
          devs.push(k);
          break;
        }
      }
    }

    console.log("DEVEVVEVVEV")
    console.log(devs)
    return devs;
  }
/* GET add form - add */
router.get('/new', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('createAny', 'routine'), function(req, res, next) {

// Get rooms and device information
  getDevicesAggregated(function(result) {
    res.render('routines/add', {
      title: 'Routines - New',
      rooms: result,
      suggestion: false
    });
  });
});


function getDevicesAggregated(callback) {
  MongoClient.connect(dbConnectionUrl,{ useUnifiedTopology: true }, function(err, db) {
    if (err) throw err;
    var dbo = db.db("EcoWatt");
    dbo.collection('Rooms').aggregate([
      { $lookup:
         {
           from: 'Devices',
           localField: '_id',
           foreignField: 'roomNo',
           as: 'roomDevices'
         }
       }
      ]).toArray(function(err, result) {
      if (err) throw err;
      db.close();
      console.log(result);

      callback(result);
    });
  });
}

/* POST create routine / */
router.post('/', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('createAny', 'routine'), function(req, res, next) {
  let routine = req.body;
  routine.status = "inactive";
  routine.homeID = res.locals.activeHome._id;

  scheduleRoutine(routine, true);
  scheduleRoutine(routine,false);

  console.log(routine);
  console.log("AYYAYAYYAYYASDYYAYASYDYADYAYSDYASDVASUDAYS");
  // Initialize db connection to the routine collection
  db.initialize(dbName, routineCollection, function(dbCollection) { // successCallback

    dbCollection.insertOne(routine, function(error, result) { // callback of insertOne
        if (error) throw error;
      });
    }, function(err) { // failureCallback
      throw (err);
  });

  res.json({msg:"done!"});
});

/** POST change routine status */
router.post('/:id/changeStatus', function(req, res, next) {
  let routineId = req.params.id;
  let status = req.body.status == 'active' ? true : false;

  db.initialize(dbName, routineCollection, function(dbCollection) {
    dbCollection.findOne({
      _id: new ObjectId(routineId)
    }, function(error, result) {
      changeRoutineStatus(result, status);
      res.status(200).json({msg:"Routine status updated!"})
    })
  });

});

/* POST favorite */
router.post('/:id/favorite', function(req, res, next) {
  let routineId = req.params.id;
  let favorite =  req.body.favorite;  
  db.initialize(dbName, routineCollection, function(dbCollection) {
    dbCollection.updateOne({
      _id: new ObjectId(routineId)
    },{
      $set: {
        favorite: favorite
      }
    }, function(error, result) {
      if (error) throw error;
      res.status(200).json({msg:"Favorite updated!"})
    });
});
});

/* POST delete room /:id/delete */
router.post('/:id/delete', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('deleteAny', 'routine'), function(req, res, next) {

  db.initialize(dbName, routineCollection, function(dbCollection) { // successCallback
    // const itemId = request.params.id;
   const itemID=req.params.id;
   console.log("Delete item with id: ", itemID);

    dbCollection.deleteOne({ _id : new ObjectId(itemID)}, function(error, result) {
    if (error) throw error;
    // res.redirect('/routines/')
    res.json({msg: "Done!"});
  });
  }, function(err) { // failureCallback
    throw (err);
  });


});

/* GET routine details - read /:id */
router.get("/:id", userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('readAny', 'routine'), function(req, res, next){

  const id = req.params.id;

  if(id.toString() === "suggested") {
    console.log("suggested routine");
    renderRoutineSuggestion(req,res);
    return;
  }
  else {
    console.log("requested routine");
  }

  db.initialize(dbName, routineCollection, function(routineDbCollection) {
    routineDbCollection.findOne({ _id : ObjectId(id)}, function(error, routine) {
      if (error) throw error;
      let routineDevices = routine.devices.map(function(device){return ObjectId(device.deviceID)})
      db.initialize(dbName, devicesCollection, function(deviceDbCollection) {
        deviceDbCollection.find({ _id : { $in : routineDevices}}).toArray(function(error, devices) {
          if (error) throw error;
          let deviceRooms = devices.map(function(device){return ObjectId(device.roomNo)});
          db.initialize(dbName, roomCollection, function(roomDbCollection) {
            roomDbCollection.find({ _id : { $in : deviceRooms}}).toArray(function(error, rooms) {
              if (error) throw error;
              routine.devices = devices.map(function(device){
                device.room = rooms.find(function(room){return room._id.toString() == device.roomNo.toString()});
                device.routineDeviceStatus = routine.devices.find(function(routineDevice){return routineDevice.deviceID.toString() == device._id.toString()}).deviceStatus;
                return device;
              });
              // res.send(routine);
              res.render('routines/read',{
                routine: routine
              });
            });
          });
        });
      });
    });
  });
});


/**
 *************************************************************************
 * Routines Suggestion implementation using Neural Network
 *************************************************************************
 */


/* RenderRoutineSuggestion()
 * This function renders the new routine page populated with the routine suggested by the NN
 */
function renderRoutineSuggestion(req, res) {

  predictRoutine(function(routine) {
    //console.log(routine);

    // Get rooms and device information
    getDevicesAggregated(function(result) {
      res.render('routines/add',{
        title: 'Routines - New',
        rooms: result,
        suggestion: true,
        routine: routine
      });
    });

  });

  updateStoredDevices();

}

/* This function requires an array of inputs and an array of outputs of the same length.
 * The NeuralNetwork is trained using these values and stored back to mongoDB
 */
function trainNeuralNetwork(inputs,outputs, callback) {

  getCurrentNeuralNetwork(function(nn) {

    if(inputs.length != outputs.length) {
      console.log("Inputs and Outputs have different length: Neural Network not trained");
      alert("Inputs and Outputs have different length: Neural Network not trained");
      return;
    }

    for(let i in inputs) {
      try {
        nn.train(inputs[i], outputs[i]);
      } catch(e) {
        console.log("Changes to devices were made: updating NN");
        return callback();
      }
    }

    db.initialize(dbName, netCollection, function(dbCollection) {
      dbCollection.replaceOne({ _id : 0}, nn, function(err) {
        if(err) throw err;
        console.log("Neural Network trained and saved into DB");
        return callback();
      });
    });
  });
}

schedule.scheduleJob('0,5,10,15,20,25,30,35,40,45,50,55 * * * *', function() {
  console.log("Training NeuralNetwork...");

  let deviceArrayPromise = new Promise(function(resolve) {
    try {
      getPoweredDevices(function(powDevices) {
        getDeviceArray(Array.from(powDevices), function(deviceArray) {
          resolve(deviceArray);
        });
      });
    } catch(e) { reject(e) }
  });

  deviceArrayPromise.catch(function(err) { throw err; })
  
  deviceArrayPromise.then(function(deviceArray) {
    let currentDate = new Date(Date.now());
    let timeArray = getTimeArray(currentDate);

    console.log(timeArray);
    console.log(deviceArray);

    trainNeuralNetwork([timeArray],[deviceArray], updateStoredDevices );
  });
});

/* scheduleRoutines
 * Retrieves the routines from DB and actually schedule them to turn ON/OFF devices
*/
scheduleRoutines(); // Always called when starting application
function scheduleRoutines() {
  db.initialize(dbName, routineCollection, function(dbCollection) { 
    dbCollection.find().toArray(function(error, result) {
      if(error) throw error;
      // console.log("Scheduling routines at startup.");
      // console.log(result);
      result.forEach(routine => {
        scheduleRoutine(routine,true);
        scheduleRoutine(routine,false);
        // console.log(routine);
      });
    });
  }, err => {throw(err)});
}

/* weekDayToIndex
 * transform an array of week days in string format to integers
*/
function weekDayToIndex(days) {
  let iDays = [];
  for (let i in days) {
    for (let j in weekday) {
      if(days[i].toUpperCase() === weekday[j].toUpperCase()) {
        iDays[iDays.length] = parseInt(j);
        break;
      }
    }
  }
  return iDays;
}
/* scheduleRoutine
 * actually schedule the routine at the specified time
*/
function scheduleRoutine(routine, isRoutineStart) {
  // console.log(routine);

  // Do not schedule unscheduled routines
  if(!routine.time) { return; }

  var rule = new schedule.RecurrenceRule();
  rule.dayOfWeek = weekDayToIndex(routine.frequency);

  let routineTime = new Date();

  // Schedule the start of the routine
  routineTime.setHours(parseInt(routine.time.substring(0,2)));  
  routineTime.setMinutes(parseInt(routine.time.substring(3,5)));

  if(!isRoutineStart) { // Schedule the end of the routine
    if(!routine.durationTime) { return; } //If routine does not have durationTime, do not schedule the ending
    routineTime.setHours(routineTime.getHours() + parseInt(routine.durationTime.split(':')[0]));
    routineTime.setMinutes(routineTime.getMinutes() + parseInt(routine.durationTime.split(':')[1]));
  }
  
  rule.hour = routineTime.getHours();
  rule.minute = routineTime.getMinutes();

  // console.log(rule);

  let job = schedule.scheduleJob(rule, function(routine){
    changeRoutineStatus(routine, isRoutineStart);
  }.bind(null, routine));
  
  console.log(routine.routineName + ": routine " + (isRoutineStart?"start":"end") + " scheduled");
  
}

/* changeRoutineStatus
 * change the status of all of the devices in the routine and the status of the routine itself 
*/
function changeRoutineStatus(routine, newRoutineStatus) {
  db.initialize(dbName, devicesCollection, function(dbCollection) { 
    for(let device of routine.devices) {
      let newDeviceStatus = device.deviceStatus.toUpperCase(); 
      if(!newRoutineStatus) {
        // newDeviceStatus = newDeviceStatus.toUpperCase() === "ON" ? "OFF" : "ON";
        newDeviceStatus = "OFF";
      }
      dbCollection.updateOne({ _id : new ObjectId(device.deviceID) },{ $set: {status:newDeviceStatus} }, function (error) {
        if (error) throw error;
        console.log(device);
        console.log("^^^^ device turned " + newDeviceStatus + " ^^^^");
      });
    }
  }, err => {throw(err)});

  db.initialize(dbName, routineCollection, function(dbCollection) {
    dbCollection.updateOne({ _id : new ObjectId(routine._id) },{ $set: {status:newRoutineStatus?"active":"inactive"} }, function (error) {
      if (error) throw error;

      console.log("routine status set to" + (newRoutineStatus?"active":"inactive"));
    });
  }, err => {throw(err)});
}

/* changeDevicesState
 * Change the state of multiple devices by their IDs
 * devices argument can be any iterable element (arrays,sets,maps, etc..)
*/
// function changeDevicesStateByID(_devices,state) {
//   let devToChange = new Set();
//   _devices.forEach(function(dev) { devToChange.add(dev) });

//   getCurrentDevicesInfo(function(stored_devices) {

//     let scheduledDevices = new Set();
//     stored_devices.forEach(function(stored_device) {
//       if(devToChange.has(stored_device._id.toString())) {
//         scheduledDevices.add(stored_device);
//       }
//     });

//     db.initialize(dbName, devicesCollection, function(dbCollection) { 
//       for(device of scheduledDevices) {
//         console.log(device);

//         // Actually setting state of device to given state on MongoDB
//         dbCollection.updateOne({ _id : new ObjectId(device._id) }, { $set: {status:state} }, function (_error, _finalResult) { 
//           if (_error) throw _error; 
//         });
//       }
//     });
//   });
// }

// =================================================
// =================================================
// =================================================
// =================================================
// =================================================


/* getPoweredDevices
 * returns an array of powered devices
*/
function getPoweredDevices(callback) {

  getCurrentDevicesInfo(function(devices) {
    let poweredDevices = new Set();
    for(device of devices) {
      if(!device.status) { continue; }

      if(device.status.toUpperCase() == 'ON') { poweredDevices.add(device); }
    }
    callback(poweredDevices);
  });

}

/* getCurrentDevicesInfo
 * returns a list of devices that exist at this point in time
*/
function getCurrentDevicesInfo(callback) {
    db.initialize(dbName, devicesCollection, function(dbCollection) {
      dbCollection.find().toArray(function(err,devices) {
        return callback(devices);
      }) 
    });
}

/* predictRoutine
 * returns the predicted routine for the current time
 */
function predictRoutine(callback) {

  getCurrentNeuralNetwork(function(net) {
    // console.log(net);

    getCurrentDevices(function(sortedDevices) {
      // console.log(sortedDevices);

      let currentDate = new Date(Date.now());
      let input = getTimeArray(currentDate);
      let prediction = net.predict(input);

      let routineData = {};
      routineData.routineName = "Suggested routine";
      routineData.time = String(currentDate.getHours()).padStart(2,'0') + ":" + String(currentDate.getMinutes()).padStart(2,'0');
      routineData.frequency = [weekday[currentDate.getDay()]];

      let highProbDevices = [];

      console.log(prediction);

      let highProbValue = 0.8; // VALUE CAN BE ADJUSTED
      let lowProbValue = 0.1; // VALUE CAN BE ADJUSTED

      for(let i in prediction) {
        // Adding device to routine if prediction is over highProbValue
        // console.log(prediction[i]);
        if(prediction[i] > highProbValue) {

          highProbDevices[highProbDevices.length] = sortedDevices[i];
        }
      }
      // console.log(highProbDevices);
      routineData.devices = highProbDevices;

      callback(routineData);
    });

  });
}

/* getTimeArray
 * returns the input array to be used to train the NN, for the time given as argument
 */
function getTimeArray(date) {

  let time_slot = 30;
  let curr_time_slot = Math.floor(date.getHours()*(60/time_slot) + date.getMinutes()/time_slot);

  let isWeekday = (date.getDay() == 6 || date.getDay() == 0) ? false : true;

  let time_array = new Matrix(50,1).toArray();

  time_array[curr_time_slot] = 1;
  time_array[isWeekday?48:49] = 1;

  // console.log(time_array);
  return time_array;
}

/* getDeviceArray
 * returns the output array to be used to train the NN, for the devices given as argument
 */
function getDeviceArray(poweredDevices, callback) {
  getCurrentDevices(function(allDevices) {

    let poweredDeviceIDs = new Set(poweredDevices.map(device => String(device._id)));
    // console.log(poweredDeviceIDs);

    let device_array = new Matrix(allDevices.length,1).toArray();

    for(let i in device_array) {
      // console.log(Object(allDevices[i]._id));
      if(poweredDeviceIDs.has(String(allDevices[i]._id))) {
        device_array[i] = 1;
      }
    }
    // console.log(device_array);

    callback(device_array);
  });
}

/* getCurrentNeuralNetwork
 * returns the current NN stored in mongoDB, if it doesn't exist, it creates a new NN
 */
function getCurrentNeuralNetwork(callback) {

  let nnPromise = new Promise(function(resolve,reject) {
    db.initialize(dbName, netCollection, function(dbCollection) {

      let findNetPromise = new Promise(function(resolve,reject) {
        dbCollection.find({ _id : 0 }).toArray(function(err,storedNet) {
          if(err) throw err;
          // console.log(storedNet.length);
          if(storedNet.length != 0) { // resolve when NeuralNetwork is already saved into MongoDB
            let net = NeuralNetwork.initNeuralNetwork(storedNet[0]);
            resolve(net);
          }
          else { // reject when NeuralNetwork is not present, a new one is then created

            getCurrentDevices(function(devices) {
              // console.log(devicesNumber);
              let struct = [50,48,24, devices.length];
              net = new NeuralNetwork(struct);
              reject(net);
            });

          } // END else statement
        }); // END collection find
      }); // END findNetPromise

      findNetPromise.catch(function(err) { throw err } );

      findNetPromise.then(
        // Resolve function
        function(net) {
          console.log("Neural Network already saved into DB");
          resolve(net);
        },
        // Reject function
        function(net) {
          dbCollection.insertOne(net, function(err) {
            if(err) throw err;
            console.log("New Neural Network saved into DB");
            resolve(net);
          });
      });
    });
  });

  nnPromise.then(function(net) { return callback(net); });
  nnPromise.catch(function(err) { throw err; });
}

/* updateStoredDevices
 * updates the list of devices stored in the NeuralNetwork collection in mongoBD
 * Used when removing or adding a new device.
 */
function updateStoredDevices(callback) {

  getCurrentDevicesInfo(function(devices) {
    db.initialize(dbName, netCollection, function(dbCollection) {

      getCurrentDevices(function(storedDevices) {

        // ======================================
        // Removing devices that do not exist anymore

        const DEFUNCT = { defunct: true };

        let newDevicesSet = new Set();
        if(devices) {
          devices.forEach(function(device) { newDevicesSet.add(device._id.toString())});
        }

        // console.log("=========== REMOVING ===========");
        for(let i in storedDevices) {
          if(!newDevicesSet.has(storedDevices[i]._id.toString())) {
            // console.log("Removing Device");    
            DEFUNCT._id = Math.random().toString(); 
            storedDevices[i] = DEFUNCT;
          }
          else { 
            // console.log(storedDevices[i]._id.toString());
          }
        }

        // ========================================
        // Adding new devices
        
        let storedDevicesSet = new Set();
        if(storedDevices) {
          storedDevices.forEach(function(device) { 
            if(!device.defunct)
              storedDevicesSet.add(device._id.toString());
          });
        }

        // console.log("=========== ADDING ===========");
        for(let i in devices) {
          if(!storedDevicesSet.has(devices[i]._id.toString())) {
            // console.log("Adding new device");

            for(let j in storedDevices) { // Trying to replace a DEFUNCT device
              if(storedDevices[j].defunct) {
                storedDevices[j] = devices[i];
                break;
              }
            }

            // Adding new device to array
            storedDevices[storedDevices.length] = devices[i];
          }
          else {
            console.log(devices[i]._id.toString());
          }
        }

        // Removing all DEFUNCT at the end of array
        for (let i = storedDevices.length-1; i >= 0; i--) {
          if(storedDevices[i].defunct) {
            storedDevices.pop();
          }
          else break;
        }

        // Storing new devices and new NN to mongoDB
        let devicesData = { _id: 1, devices: storedDevices};
        dbCollection.replaceOne({ _id: 1}, devicesData , { upsert: true }, function(err1) {  
          if(err1) throw err;
          getCurrentNeuralNetwork(function(net) {
            let new_net = NeuralNetwork.initNeuralNetwork(net, storedDevices.length);
            dbCollection.replaceOne({ _id: 0}, new_net , { upsert: true }, function(err2) {  
              if(err2) throw err;
              if(callback) return callback();
            })
          });
        })
      })
    });
  });
}

/* getCurrentDevices
 * returns an array of the devices that the NN maps to
 */
function getCurrentDevices(callback)  {
  db.initialize(dbName, netCollection, function(dbCollection) {
    dbCollection.find({ _id: 1 }).toArray(function(err,result) {

      if(err) throw err;

      // console.log(result[0].devices);
      // console.log("=========================");
      if(result[0].devices) {
        return callback(result[0].devices);
      }
      else {
        return callback([]);
      }
    });
  });
}

/* getDailyProbabilities
 * Returns all the daily probabilities of all devices in a 30 minutes intervals
*/
function getDailyProbabilities(callback) {
  getCurrentNeuralNetwork(function(net){

    let consumptionMatrix = [];
    let timeArray = [];

    for (let i = 0; i < 24; i++) {
      let time = new Date();
      time.setHours(i);
      time.setMinutes(0);
      timeArray.push(moment(time).format('HH:mm'));

      let time_array = getTimeArray(time);

      prediction = net.predict(time_array);
      consumptionMatrix[i] = prediction;

    }
    // console.log(consumptionMatrix);
    // console.log(timeArray);
    return callback(consumptionMatrix, timeArray);
  });
}


module.exports = router;
