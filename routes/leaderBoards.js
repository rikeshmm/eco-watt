var express = require('express');
var router = express.Router();
var moment = require('moment');
var _ = require('lodash');

const MongoClient = require("mongodb").MongoClient;
var ObjectId = require('mongodb').ObjectID;
const dbConnectionUrl = "mongodb+srv://ak125:ecowatt2020@cluster0-cjt2c.mongodb.net/test?retryWrites=true&w=majority";

/* GET users listing. */
router.get('/', function(req, res, next) {

    var d = new Date();
    var data = {};
    data.date = moment(d).format('ll'); 

    MongoClient.connect(dbConnectionUrl, function(err, db) {
   
        if (err) throw err;
        var dbo = db.db("EcoWatt");

      dbo.collection('Consumption').aggregate([
       { $project: {
            userID: {
              $toObjectId: "$user"
            },
            date : "$date",
            homeID : "$homeID",
            energy : "$energy"
          }
        },
        { $lookup:
            {
              from: 'Users',
              localField: 'userID',
              foreignField: '_id',
              as: 'userConsumption'
            }
          },
          
        {$match: { $and: [ { date: data.date.toString() }
             , { homeID : res.locals.activeHome._id} 
            ] }  },
        { 
          $group :
            {
              _id : "$userID",
              energySum: { $sum: "$energy" },
              user : { $addToSet : "$userConsumption"}
            }
        },
        {
            $sort : { energySum: 1 }
          }
        ]).toArray(async function(err, result) {
  
          if (err) throw err;

          data.energy = result; 
          data.currUser = res.locals.activeUser._id;

          db.close();      
          console.log("YOZZZ")

            // console.log(_.flattenDeep(data.energy[0]))
            console.log(_.flattenDeep(data.energy[0].user))

          res.render('leaderBoards/browse', {
              
            result: data

          });

    
    });
    
});

});



module.exports = router;

