const express = require('express');
const router = express.Router();

// DB Require
const db = require("../db");
const dbName = "EcoWatt";
const ObjectId = require('mongodb').ObjectID;
const userCollection = "Users";
const deviceCollection = "Devices";
const routineCollection = "Routines";

const nodemailer = require("nodemailer");
const Mail = require('../mail');

// Middleware
const userMiddleware = require('../middleware/user');

/**
 * Users
 * GET route to browse user profiles
 */
router.get('/browse', userMiddleware.allowIfLoggedin, userMiddleware.allowIfNotActiveUser, function(req, res, next) {
  // Get the home from cookies
  const home = req.cookies.home;

  // Query database to get user information
  db.initialize(dbName, userCollection, function (userDbCollection) {
    userDbCollection
    .find({ 
      homeId : new ObjectId(home._id)
    })
    .toArray(function (error, users){
      if (error) throw error;
      return res.render('users/browse', {
        title: 'User - browse',
        users : users,
      });
    });
  });
});

/**
 * Users
 * POST route to set the active user profile 
 */
router.post('/browse', userMiddleware.allowIfLoggedin, userMiddleware.allowIfNotActiveUser, function(req, res, next) {
  // Get the user id
  const userId = req.body.userId;

  // Query database to get user information
  db.initialize(dbName, userCollection, function (userDbCollection) {
    userDbCollection
    .findOne({ _id : new ObjectId(userId)}, function(error, user) {
      if (error) throw error;
      return res.status(200)
      .cookie('activeUser', user)
      .json({
        activeUser: user,
      }); 
    });
  });
});

/**
 * Users
 * GET route to manage user profiles
 */
router.get('/manage', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('updateAny', 'profile'), function(req, res, next) {
  // Get the home from cookies
  const home = req.cookies.home;

  // Query database to get user information
  db.initialize(dbName, userCollection, function (userDbCollection) {
    userDbCollection
    .find({ 
      homeId : new ObjectId(home._id)
    })
    .toArray(function (error, users){
      if (error) throw error;
      return res.render('users/manage', {
        title: 'User - manage',
        users : users,
      });
    });
  });
});

 /**
 * Users
 * POST route to add new user profile 
 */
router.post('/', function(req, res, next) {
  // Get username
  const username = req.body.username;

  // Get role
  const role = req.body.role;

  // Get home id
  const homeId = req.cookies.home._id;

  // Get avatar color
  const avatarColor = req.body.avatarColor;

  // User object
  const user = {
    username: username,
    role: role,
    avatarColor: avatarColor,
    homeId: new ObjectId(homeId),
    settings: {darkmode : false},
  }

  // Insert new home user
  db.initialize(dbName, userCollection, function (userDbCollection) {
    userDbCollection
    .insertOne(user, function(error, newUser) {
      let insertedUser = newUser.ops[0];
      console.log(insertedUser);
      var mail = new Mail("newuser", req.cookies.loggedInUser.email, "A new user has been added to your house!", {name: req.cookies.loggedInUser.username, username : insertedUser.username, permission: insertedUser.role, addedBy: req.cookies.activeUser.username});
      res.redirect("/users/manage");
    });
  });
});

/**
 * Users
 *  GET route to display add new user profile 
 */
router.get('/add', function(req, res, next) {
  return res.render('users/add', {
    title: 'User - add',
  });
});


/**
 * Users
 * GET route to view selected user profile edit page
 */
router.get('/:id/edit', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('updateAny', 'profile'), function(req, res, next) {
  // Get the user id from the URL parameters
  const userId = req.params.id;

  // Get the home from the cookies
  const home = req.cookies.home;

  // Get the logged in user
  const loggedInUser = req.cookies.loggedInUser;

  // Change role
  // This prevents the home owner to change there own role
  const changeRole = loggedInUser._id != userId ? true : false;

  // Query database to get user information
  db.initialize(dbName, userCollection, function (userDbCollection) {
    userDbCollection
    .findOne({ _id : new ObjectId(userId)}, function(error, user) {
      if (error) throw error;
      return res.render('users/edit', {
        title: 'User - edit',
        user: user,
        homeAddress: `${home.address.streetAddress}, ${home.address.secondaryAddress}`,
        changeRole: changeRole,
      });
    });
  });
});


/**
 * Users
 * GET route to view selected user profile data collection page
 */
router.get('/datacollection', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, function(req, res, next) {
  let datacollectionFrequency = req.cookies.dataCollectionFrequency;
  res.render('users/datacollection', {
    datacollectionFrequency: datacollectionFrequency
  });
});

/**
 * Users
 * POST route to update selected user profile data collection page
 */
router.post('/datacollection', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, function(req, res, next) {
  let datacollectionFrequency = req.body.datacollectionFrequency;
  res.
  cookie('dataCollectionFrequency', datacollectionFrequency)
  .redirect('/');
});

/**
 * Users
 * GET route to view selected user profile accessibility page
 */
router.get('/:id/accessibility', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, function(req, res, next) {
  // Get the user id from the URL parameters
  const userId = req.params.id;

  // Query database to get user information
  db.initialize(dbName, userCollection, function (userDbCollection) {
    userDbCollection
    .findOne({ _id : new ObjectId(userId)}, function(error, user) {
      if (error) throw error;
      return res.render('users/accessibility', {
        title: 'User - accessibility',
        user: user
      });
    });
  });
});

/**
 * Users
 * POST route to update selected user profile accessibility
 */
router.post('/:id/accessibility', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, function(req, res, next) {
  // Get user id
  let userId = req.params.id;

  // Get updated theme
  let updatedTheme = req.body.colorTheme;

  // Check if dark mode
  let isDarkMode = updatedTheme == 'dark' ? true : false;

  console.log(userId, updatedTheme, isDarkMode);

  // Find and update the user
  db.initialize(dbName, userCollection, function (userDbCollection) {
    userDbCollection
    .findOneAndUpdate({
      _id: new ObjectId(userId)
    },{
      $set : {
        'settings.darkmode' : isDarkMode
      }
    },{
      returnOriginal : false,
    }, function(error, updatedUser) {
      if (error) throw error;
      res
      .cookie('activeUser', updatedUser.value)
      .redirect('/');
    })
  });
});

/**
 * 
 */
router.post('/:id/delete', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('deleteAny', 'profile'), function(req, res, next) {
    // Get the user id from the URL parameters
    const userId = req.params.id;

    db.initialize(dbName, userCollection, function(dbCollection) {
      dbCollection.deleteOne({ _id : new ObjectId(userId)}, function(error, result) {
        if (error) throw error;
        res.redirect('/users/manage');
      });
    });
});

/**
 * Users
 * POST route to update selected user profile
 */
router.post('/:id', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('updateAny', 'profile'), function(req, res, next) {
  // Get the user id from the URL parameters
  const userId = req.params.id;

  // Get username
  const username = req.body.username;

  // Get user role
  const role = req.body.role;

  // Get avatar color
  const avatarColor = req.body.avatarColor;

  // Find and update the user
  db.initialize(dbName, userCollection, function (userDbCollection) {
    userDbCollection
    .findOneAndUpdate({
      _id: new ObjectId(userId)
    },{
      $set : {
        username: username,
        role: role,
        avatarColor: avatarColor
      }
    }, function(error, updatedUser) {
      if (error) throw error;
      res.redirect(`/users/${req.cookies.activeUser._id}`);
    })
  });
});

/**
 * Users
 * GET route to read/view selected user profile
 */
router.get('/:id', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('readOwn', 'profile'), function(req, res, next) {
  // Get the user id from the URL parameters
  const userId = req.params.id;

  // Get the home from the cookies
  const home = req.cookies.home;

  // Check if the user requesting is the user being requested for
  const activeUser = req.cookies.activeUser;
  if (activeUser._id !== userId) {
    return res.status(401).json({
      error: "You are not the owner of the resource"
      });
  }

  // Promise to count users
  let userCountPromise = new Promise(function(resolve, reject){
    db.initialize(dbName, userCollection, function (userDbCollection) {
      userDbCollection
      .find({
        homeId : new ObjectId(req.cookies.home._id)
      })
      .count(function(error, count){
        error ? reject(error) : resolve(count);  
      })
    })
  });

  // Promise to count devices
  let deviceCountPromise = new Promise(function(resolve, reject){
    db.initialize(dbName, deviceCollection, function (deviceDbCollection) {
      deviceDbCollection
      .find({
        homeID : req.cookies.home._id
      })
      .count(function(error, count){
        error ? reject(error) : resolve(count);  
      })
    })
  });

  // Promise to count routines
  let routineCountPromise = new Promise(function(resolve, reject){
    db.initialize(dbName, routineCollection, function (routineDbCollection) {
      routineDbCollection
      .find({
        homeID : req.cookies.home._id
      })
      .count(function(error, count){
        error ? reject(error) : resolve(count);  
      })
    })
  });

  // Promise to get selected user
  let userPromise = new Promise(function(resolve, reject){
    db.initialize(dbName, userCollection, function (userDbCollection) {
      userDbCollection
      .findOne({
        _id : new ObjectId(userId)
      }, function(error, user) {
        console.log(user);
        error ? reject(error) : resolve(user);  
      })
    })
  });

  Promise
  .all([userCountPromise, deviceCountPromise, routineCountPromise, userPromise])
  .then(function(result){
    return res.render('users/read', {
      title: 'User - Read',
      user: result[3],
      userCount: result[0],
      deviceCount: result[1],
      routineCount: result[2],
      homeAddress: `${home.address.streetAddress}, ${home.address.secondaryAddress}`
    });
  });
});

module.exports = router;
