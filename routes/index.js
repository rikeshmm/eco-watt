var express = require('express');
var router = express.Router();
const Noty = require('noty');
var ObjectId = require('mongodb').ObjectID;
const axios = require('axios');
var moment = require('moment');
var _ = require('lodash');

const db = require("../db");
const dbName = "EcoWatt";
const userCollection = "Users";
const roomCollection = "Rooms";
const deviceCollection = "Devices";
const routineCollection = "Routines";
const energyConsumed = "EnergyConsumed";
const netCollection = "NeuralNetwork";
const globalEnergyCollection = "GlobalEnergy";
const scoresCollection = "Scores";
const consumptionCollection = "Consumption"
const solarPanelsCollection = "SolarPanels"

// New way of initialising and using MongoDB
// let dbObject;
// db.initialize(dbName, deviceCollection, function(dbCollection, dbO) {
//   dbObject = dbO;
// }, function(error) {
//   throw error;
// });



// const Artyom = require('artyom.js').default;
// const Eco = new Artyom();

const userMiddleware = require('../middleware/user');

const NeuralNetwork = require('../public/net/net');
const Matrix = require('../public/net/matrix');
const SolarPanel = require('../public/javascripts/solarpanel');

const schedule = require('node-schedule');

var ObjectId = require('mongodb').ObjectID;
const MongoClient = require("mongodb").MongoClient;
const dbConnectionUrl = "mongodb+srv://ak125:ecowatt2020@cluster0-cjt2c.mongodb.net/test?retryWrites=true&w=majority";

async function getResponse(score, status, homeID, message, username, callback){

  console.log("456789o")
  console.log(status)
  console.log(score)

  var words = message.split(" ");
  var len = words.length;
  console.log(words);

  //Switching ON/OFF device
  if(words.includes("score") || words.includes("doing")){
 
    
    var successCallback = "Your energy score is " + score + ". You are in the " + status + " zone.";
      callback(successCallback); 

  }
  else if(words.includes("eco") || words.includes("Eco")){
  
      var successCallback = "Hey " + username + ". How may I help you?"
      callback(successCallback); 

  }
  else if(words[0]=="switch" || words[0]=="turn"){
    console.log("else if called");
    
    var state = words[1].toUpperCase();

  let dev = words[2];
  console.log("qlwdhkbrker");
  console.log(words);

  console.log(dev);

  for (let i = 3; i < words.length; i++) {

    if(words[i]=="one"){
        dev += " 1";
      }
      else if(words[i]=="two"){
        dev += " 2";
      }
      else if(words[i]=="three"){
        dev += " 3";
      }
      else if(words[i]=="four"){
        dev += " 4";
      }
      else if(words[i]=="five"){
        dev += " 5";
      }else if(words[i]=="six"){
        dev += " 6";
      }else{
        console.log("else called", dev, words[i]);
        dev += " ";
        dev  +=  words[i];
      }
      console.log(dev);

  }


  const devices = await getDeviceArray();

  console.log("YOLOOOOO")
  console.log(devices)

  dev = dev.toLowerCase();

  console.log(dev);
  console.log("=========================")

  console.log(devices);

  db.initialize(dbName, deviceCollection, async function(dbCollection) {


   let newDev = await returnDev(devices , dev);
  console.log("=========================")

   console.log(newDev)


    dbCollection.updateOne({ _id : newDev.devID },{ $set: {status:state} }, function(error, result) {
        console.log("YAYYYYYYY!"); // failureCallback
        var successCallback = newDev.devName + " switched " + state;
        callback(successCallback);
        if (error) throw error;
    });

  });


  }else{
    var successCallback = "Invalid Query!";
        callback(successCallback);
  }
}

async function getDeviceArray() {
  const client = await MongoClient
  .connect(dbConnectionUrl, { useNewUrlParser: true })
  .catch(err => { console.log(err); });
  if (!client) {
    return;
  }
  try {
    const dbConnect = client.db(dbName);
    let collection = dbConnect.collection(deviceCollection);
    let res = await collection.find().toArray();
    return res;
  } catch (error) {
    console.log(error);
  } finally {
    client.close();
  }
}

router.use(async function(req, res, next) {
  console.log("============INDEX MIDDLEWARE BOI============");
  let homeID = req.cookies.home;
  let dataCollectionFrequency = req.cookies.dataCollectionFrequency;
  if (homeID) {
    produceSolarEnergy(homeID._id, dataCollectionFrequency);
  }
  next();
  console.log("============================================");
})


async function returnDev( devices , dev ){

  var newDev={};
 for (let i = 0; i < devices.length; i++) {
   let checkDev = devices[i].deviceName.toLowerCase();
  console.log(checkDev)
  if(checkDev == dev.toLowerCase()){
    console.log(devices[i], "lol");
    newDev.devID = devices[i]._id;
    newDev.devName = devices[i].deviceName.toLowerCase();
  }
  
 }

 return newDev;
}
/**
 *************************************************************************
 * Dashboard
 *************************************************************************
 */

/**
 * Dashboard
 * GET route to display the dashboard
 */
router.get('/', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('readAny', 'home'), function (req, res, next) {

  // Promise to retrieve rooms from the database
  var rooms = new Promise(function(resolve, reject) {
    db.initialize(dbName, roomCollection, function(dbCollection) {
      dbCollection.aggregate([
        { $match:
          { $and: [ {favorite: true}
            , { homeID : res.locals.activeHome._id} 
           ] }
        },
        { $lookup:
           {
             from: 'Devices',
             localField: '_id',
             foreignField: 'roomNo',
             as: 'roomDevices'
           }
         }
        ]).toArray(function(error, result) {
          // console.log(result);

          error ? reject(error) : resolve(result);
        });
    }, function(err) {
      throw (err);
    });
  });

  // Promise to retrieve routines from the database
  var routines = new Promise(function(resolve, reject) {
    db.initialize(dbName, routineCollection, function(dbCollection) {
      dbCollection.find({ $and: [ {favorite: true}
        , { homeID : res.locals.activeHome._id} 
       ] }).toArray(function(error, result) {
        error ? reject(error) : resolve(result);
      });
    }, function(err) {
      throw (err);
    });
  });

  // Resolve all promises to get rooms and routines from the database
  Promise.all([rooms,routines]).then(function(result) {
    // console.log(result[0]);
    res.render('index', {
      title: 'Home',
      rooms: result[0],
      routines: result[1]
    });
  });

});

/* createSolarPanels
 * function called to add new solar panels to the house
*/
// createSolarPanels(()=>{});
function createSolarPanels(callback) {
  db.initialize(dbName, solarPanelsCollection, function(dbCollection) {
    dbCollection.drop();

    // ====== TUNABLE ======
    let solarPanelsCount = 50;
    // ^^^^^^^^^^^^^^^^^^^^^
    
    let solarPanels = [];
    for (let i = 0; i < solarPanelsCount; i++) {
      let capacity = 100000;
      let wattGenerated = 1;
      solarPanels[i] = new SolarPanel(i,capacity,wattGenerated,0);
    }

    try {
      dbCollection.insertOne( {
        _id: 0,
        solarPanels: solarPanels
      }, function(){ //callback of insertOne
        return callback(solarPanels);
      });
    } catch (e) {
      console.log(e);
    }

  }, function(err) {
    throw (err);
  });
}

/* retrieveSolarPanels
 * function to retrieve current solar panels and their info from DB
*/
function retrieveSolarPanels(dbCollection, callback) {
  dbCollection.find({}).toArray(function(error,result) {
    if(error) throw error;
    
    let solarPanelsData = result[0].solarPanels;
    let solarPanels = [];
    let isValidData = true;

    for (let i in solarPanelsData) {

      if(solarPanelsData[i].wattGeneration == null || isNaN(solarPanelsData[i].stored_energy)) { isValidData = false; break;}
      let capacity = solarPanelsData[i].capacity;
      let wattGeneration = solarPanelsData[i].wattGeneration;
      let stored_energy = solarPanelsData[i].stored_energy;
      let panelID = solarPanelsData[i]._id;
      solarPanels[i] = new SolarPanel(panelID, capacity, wattGeneration, stored_energy);
      // console.log(solarPanels[i]);
    }

    // console.log(solarPanels);

    if(isValidData) {
      return callback(solarPanels);
    }
    else {
      createSolarPanels(callback);
    }
    
  });
}

/* storeSolarPanels
 *
*/
function storeSolarPanels(dbCollection, solarPanels) {
  dbCollection.replaceOne({ _id: 0 }, {
        _id: 0,
        solarPanels: solarPanels
      }, function(err) {
    if(err) throw err;
  });
}

  async function getDevices() {
    const client = await MongoClient
    .connect(dbConnectionUrl, { useNewUrlParser: true })
    .catch(err => { console.log(err); });
    if (!client) {
      return;
    }
    try {
      const dbConnect = client.db(dbName);
      let collection = dbConnect.collection(deviceCollection);
      let query = { status: "ON" }
      let res = await collection.find(query).toArray();
      return res;
    } catch (error) {
      console.log(error);
    } finally {
      console.log("Connection Closed");
      client.close();
    }
  }

  async function getRoutines() {
    const client = await MongoClient
    .connect(dbConnectionUrl, { useNewUrlParser: true })
    .catch(err => { console.log(err); });
    if (!client) {
      return;
    }
    try {
      const dbConnect = client.db(dbName);
      let collection = dbConnect.collection(routineCollection);
      let query = { status: "active" }
      let res = await collection.find(query).toArray();
      // console.log("res")
      // console.log(res)
      return res;
      
    } catch (error) {
      console.log(error);
    } finally {
      console.log("Connection Closed");
      client.close();
    }
  }
  
  async function routineArray() {
 
    let routineDevices = [];
    
    let routines = await getRoutines();
    // console.log("routines")
    // console.log(routines)

    routines.forEach(routine => {
  
      routine.devices.forEach(device => {

        if(device.deviceStatus== "on"){
          device.routineID = routine._id;
          routineDevices.push(device);
        }
        
      });
    });

    return _.flattenDeep(routineDevices);
  }


  async function getData() {

    data = {};
    var d = new Date();
    var data = {};
    data.time = moment(d).format('HH:mm');
    data.day =  moment(d).format('d');
    data.date = moment(d).format('ll');

    let consumptionArray = []; 
    
    let activeDevices = await getDevices();
    // console.log("ACTOVE")

    let routineDevices = await routineArray();
    // console.log(routineDevices);
    routineDevs = [];

    // routineDevices.forEach(device => {
    //   // let routineTemp = {};
    //   // routineTemp.routineID = 
    //   routineDevs.push(new ObjectId(device.deviceID));
    // });
 

    if(activeDevices != null){

   activeDevices.forEach(dev => {

    let newDev = {};
    newDev.time = data.time;
    newDev.date = data.date;
    newDev.day = data.day; 
      
    routineDevices.forEach(element => {
      // console.log(element.deviceID, dev._id,  element.deviceID == dev._id);
      
      if(element.deviceID.toString() == dev._id.toString()){
        // console.log("lalala incldes")
        newDev.fromRoutine = true;
        newDev.routineID = element.routineID;
      }
    });

      if(routineDevs.includes(dev._id)){
        // console.log("lalala incldes")
        newDev.fromRoutine = true
      }

      newDev.deviceName = dev.deviceName;
      newDev.deviceID = dev._id;
      newDev.roomID = dev.roomNo;
      newDev.energy =  Math.floor(Math.random() * (20 - 5 + 1)) + 5;
      newDev.user = dev.user;
      newDev.homeID = dev.homeID;
      consumptionArray.push(newDev);

    });

  }
    return consumptionArray;

  }

/* produceSolarEnergy
 * change the energy produced on all solar panels and stores result into DB
*/

// produceSolarEnergy("5e985d85b98ac707ed22b1c7"); // This calls the function at node.js startup
 function produceSolarEnergy(homeID, dataCollectionFrequency) {
  console.log("produceSolarEnergy", homeID, dataCollectionFrequency);
  db.initialize(dbName, solarPanelsCollection, function(dbCollection) {
    retrieveSolarPanels(dbCollection, function(solarPanels) {
      let totalEnergyProduced = 0;
      
      for (let i in solarPanels) {
        totalEnergyProduced += solarPanels[i].produce();
        // console.log(solarPanels[i]);
        // console.log(totalEnergyProduced);
      }
      console.log("total energy produced: " + totalEnergyProduced);

      MongoClient.connect(dbConnectionUrl, async function(err, db) {
        if (err) throw err;
        var dbo = db.db("EcoWatt");
        
        data = {};
        var d = new Date();
        var data = {};
        data.time = moment(d).format('HH:mm');
        data.day =  moment(d).format('d');
        data.date = moment(d).format('ll');
        data.energy = totalEnergyProduced;
        data.homeID = homeID;
      
        dbo.collection('Generation').insertOne(data, function(error, result) { // callback of insertOne
          if (error) throw error;
          // console.log(data);
          
        }, function(err) { // failureCallback
          throw (err);
        
      });

      dbo.collection('GlobalEnergy').updateOne({ _id : new ObjectId("5e71f19d1c9d440000fa4596") },{$inc: { generatedEnergy: totalEnergyProduced }}, function (error, result) {
        if (error) throw error;
        
        }, function(err) { // failureCallback
        throw (err);
        });

     
      let consumptionArray = await getData();

      if(consumptionArray != null){
        // console.log("Ths is consumption array")
        // console.log(consumptionArray)
      dbo.collection('Consumption').insertMany(consumptionArray, function(error, result) { 
        if (error) throw error;

      }, function(err) { // failureCallback
        throw (err);
      
      });
    }
      
    }); 

      // console.log(solarPanels)
      storeSolarPanels(dbCollection,solarPanels);

      console.log("Solar Panels have produced " + totalEnergyProduced + " Kw.");

      let freq = parseInt(dataCollectionFrequency) ? parseInt(dataCollectionFrequency) : 15;
      setTimeout(produceSolarEnergy, 60000 * freq, homeID, dataCollectionFrequency); // timeout is 15 minutes
    });
  }, function(err) {
    console.log(err);
  });
}

/* generateScores
 * Function to generate scores of other houses in given region
*/
function generateScores() {
  db.initialize(dbName, scoresCollection, function(dbCollection) {
    
    dbCollection.remove();

    // ========= TUNABLE ========= 
    let scoresCount = 1000;
    let sparsity = 200000;
    let region = "UK";
    // ^^^^^^^^^^^^^^^^^^^^^^^^^^^

    let newScores = [];
    for(let i = 0; i < scoresCount; i++) {
      let scoreData = {
        score: (Math.random() - 0.5) * sparsity,
        region: region
      }
      newScores[newScores.length] = scoreData;
    }

    try {
      dbCollection.insertMany(newScores);
    } catch (e) {
      console.log(e);
    }
  }, function(err) {
    throw (err);
  });
}

router.post('/', function (req, res, next) {
      res.render('index', {
        title: 'Home',
        qs: req.body
      });
});
// TEST ROUTES
router.get('/splashscreen', function (req, res, next) {
  res.render('splashscreen', {
    title: 'EcoWatt',
  });
});


router.get('/voice', function (req, res, next) {
  res.render('voice', {
    title: 'Voice',
  });
});

router.post('/voice', function (req, res, next) {

  console.log("VOICE API",req.body);
  
	getResponse(req.body.score, req.body.status ,req.body.homeID,req.body.message,req.cookies.activeUser.username, function(response){
		console.log(response)
		res.json({
      message: response,
		})
	});

});

router.get('/profile', function (req, res, next) {
  res.render('profile', {
    title: 'Profiles',
  });
});

router.get('/devadd', function (req, res, next) {
  res.render('devadd', {
    title: 'devices',
  });
});

router.get('/qrc', function (req, res, next) {

  res.render('qrc', {
    title: 'qrc',
    room: req.query.room
  });

});

router.get('/User', function (req, res, next) {
  res.render('User', {
    title: 'User',
  });
});

module.exports = router;
