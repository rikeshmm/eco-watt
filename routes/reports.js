var express = require('express');
var router = express.Router();
var moment = require('moment');
var sleep = require('system-sleep');
var clone = require('clone');
const multer = require('multer');

const nodemailer = require("nodemailer");
const Mail = require('../mail');

// Multer setup
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads')
  },
  filename: function (req, file, cb) {
    cb(null, 'EcoWatt-' + Date.now() + '.png')
  }
});

var upload = multer({ storage: storage });

// var Sync = require('sync');
// moment().format('llll');

const MongoClient = require("mongodb").MongoClient;
var ObjectId = require('mongodb').ObjectID;
const dbConnectionUrl = "mongodb+srv://ak125:ecowatt2020@cluster0-cjt2c.mongodb.net/test?retryWrites=true&w=majority";
var _ = require('lodash');

const db = require("../db");
const dbName = "EcoWatt";
const deviceCollection = "Devices";
const roomCollection = "Rooms";
const userCollection = "Users";
const consumptionCollection = "Consumption";
const routineCollection = "Routines";
const consumption = "EnergyConsumed";
const generation = "EnergyGenerated";

var array=[];

/* GET rooms listing. */
router.get('/', function(req, res, next) {
  var updates = [];

    // var myVar = setInterval(myTimer, 10000);

    var d = new Date();
    var data = {};
    data.time = moment(d).format('HH:mm');
    data.day =  moment(d).format('d');
    data.date = moment(d).format('ll');
    
    var gen ={};
    gen.time = moment(d).format('HH:mm');
    gen.day =  moment(d).format('d');
    gen.date = moment(d).format('ll');

    var format = 'HH:mm';

    const weekday=[0,1,2,3,4,5];
    const weekend= [6,7];
    const morningPower = ['ON','OFF','ON','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','ON','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF','OFF'];

    const eveningPower = ['OFF', 'OFF', 'ON', 'ON', 'ON','ON', 'OFF','ON', 'ON','ON','OFF','ON','ON','ON','ON','ON','ON','ON','ON','ON','ON','ON','ON','ON','ON','ON','OFF'];
    var weekdayTime = moment(data.time,format),
        beforeTime = moment('08:00', format),
        afterTime = moment('17:00', format);

    var weekendTime = moment(data.time,format),
        beforeTime = moment('17:00', format),
        afterTime = moment('22:00', format);

        if(data.time == '00:00'){
          totalPower=0;
          gen.saving = -15000;

        }

     var deviceArray={};
     var power;
     var capacity = 15000;
     var totalPower = 20;
    gen.saving = 0;
  
     
    let step1=  new Promise(function(resolve, reject){
    db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback

      dbCollection.find().toArray(function(_error, _result) {
        if (_error) throw _error;
        
        _error ? reject(_error) : resolve(_result);
         
      });

    });
  });

  step1.then(function(result){

    array = result;
    console.log("#########");
    console.log(array);

    var i; count=0;
    var insertArray=[];

      for(i=0;i<100;i++){

         count++;

            array.forEach(element => {

            power = morningPower[Math.floor(Math.random() * morningPower.length)];
            console.log(power);
            if(power=='ON'){
              data.energy =   Math.floor(Math.random() * (10 - 5 + 1)) + 5;
            }
            else{
              data.energy = 0;
            }

            data.deviceID = element._id;
            data.deviceName = element.deviceName;
            data.roomID = element.roomNo;
            console.log("@@@@@@@@@@@");
            console.log(data);
          
           if(data.energy != 0){
            temp = clone(data);

            insertArray.push(temp);
            // data=null;

           }

            });
 
            // }
          // setTimeout(2000); //wait ten seconds before continuing
 
        data.time = moment(data.time, 'HH:mm').add(15,'minutes').format('HH:mm');
        // console.log("Yozz" + data.time);
        if(count % 96 == 0){
         data.day = moment(data.day, 'd').add(1, 'day').format('d');
          data.date = moment(data.date, 'll').add(1, 'day').format('ll');
        }

    }

    console.log(insertArray);
    // let step2 = new Promise(function(resolve, reject){

      db.initialize(dbName, consumption, function(dbCollection) { // successCallback
        dbCollection.insertMany(insertArray)
        .then(result => {
          console.log(`Successfully inserted ${result.insertedIds.length} items!`);
          // error ? reject(_error) : resolve(_result);
        })
        .catch(err => console.error(`Failed to insert documents: ${err}`))
    });

    // });

  });
    
   

});

router.get('/deviceReport/', function(req, res, next) {

  res.render('reports/deviceReport', {
    title: 'Report',
    deviceID : req.query.deviceID,
    deviceName : req.query.deviceName
  });
});

router.get('/roomReport/', function(req, res, next) {

  res.render('reports/roomReport', {
    title: 'Report',
    roomName : req.query.roomName,
    roomID : req.query.roomID
    
  });
});

router.get('/allRoomsReport/', function(req, res, next) {

  res.render('reports/allRoomsReport', {
    title: 'Report',
  });
});

router.get('/peakTimeReport/', function(req, res, next) {

  res.render('reports/peakPrediction', {
    title: 'Report',
  });
});

router.get('/allRoutinesReport/', function(req, res, next) {

  res.render('reports/allRoutinesReport', {
    title: 'Report',
  });
});

router.get('/houseReport/', function(req, res, next) {

  res.render('reports/houseReport', {
    title: 'Report',
  });
});

router.get('/userReport/', function(req, res, next) {

  res.render('reports/userReport', {
    title: 'Report',
  });
});

router.get('/eachDeviceReport/', function(req, res, next) {

  res.render('reports/eachDeviceReport', {
    title: 'Report',
  });
});


router.get('/roomDevicesReport/', function(req, res, next) {

  res.render('reports/roomDevicesReport', {
    title: 'Report',
  });
});

router.post('/share', upload.single("picture"), function(req, res, next) {
  console.log("=================");
  console.log(req.body);
  console.log("=================");
  console.log(req.file);
  console.log("=================");
  
  res.status(200).json(req.file);
});

router.post('/email', upload.array('pictures', 2), function(req, res, next) {
  console.log("=================");
  console.log(req.body);
  console.log(req.files);
  console.log("=================");
  
  let reportTitles = req.body.titles;
  let reports = req.files;
  
  let mailAttachments = [];
  reports.forEach(function(report){
    mailAttachments.push({
      filename: report.filename,
      path: report.path,
      cid: report.filename
    })
  });

  let mailReports = [];
  for (let i = 0; i < reportTitles.length; i++) {
    mailReports.push({
      title: reportTitles[i],
      filename: reports[i].filename
    })
  }

  var mail = new Mail("homereport", req.cookies.loggedInUser.email, "Your Energy Consumption Report", {name: req.cookies.loggedInUser.username, reports: mailReports, date: new Date()}, mailAttachments);

  res.status(200).json(req.files);
});

module.exports = router;