var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;

const userMiddleware = require('../middleware/user');

const MongoClient = require("mongodb").MongoClient;

const dbConnectionUrl = "mongodb+srv://ak125:ecowatt2020@cluster0-cjt2c.mongodb.net/test?retryWrites=true&w=majority";

const db = require("../db");
const dbName = "EcoWatt";
const deviceCollection = "Devices";
const roomCollection = "Rooms";


/* GET rooms listing - browse */ 
router.get('/', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('readAny', 'room'), function(req, res, next) {
  MongoClient.connect(dbConnectionUrl, function(err, db) {
    if (err) throw err;
    var dbo = db.db("EcoWatt");
    dbo.collection('Rooms').aggregate([
      { $lookup:
         {
           from: 'Devices',
           localField: '_id',
           foreignField: 'roomNo',
           as: 'roomDevices'
         }
       }
       ,{
         $match :{
           homeID : req.cookies.home._id
         }
       }
      ]).toArray(function(err, result) {
      if (err) throw err;
      console.log("##################################################");
      db.close();
      // console.log(JSON.stringify(result));
      res.render('rooms/browse', {
        rooms: result,
      });
    });
  });
  
  
});

/* GET add form - add */
router.get('/new', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('createAny', 'room'), function(req, res, next) {

    res.render('rooms/add', {
    title: 'Rooms - New',
  });

});

/* POST create room / */
router.post('/', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('createAny', 'room'), function(req, res, next) {

  console.log(req.body);
  
  var room = {
    roomName: req.body.roomName,
    roomIcon: req.body.myselect,
    houseID : res.locals.activeHome._id,
    favorite: true 
  }
  min = Math.ceil(12);
  max = Math.floor(30);
  room.roomTemp = Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 

  min = Math.ceil(50);
  max = Math.floor(88);
  room.humidity =  Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 

  min = Math.ceil(124);
  max = Math.floor(170);
  room.lightIntensity= Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 

  min = Math.ceil(50);
  max = Math.floor(100);
  room.power = Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the 


  db.initialize(dbName, roomCollection, function(dbCollection) { // successCallback
 
  dbCollection.insertOne(room, function(error, result) { // callback of insertOne
      if (error) throw error;
      console.log(result.insertedId);
      res.redirect('/rooms/'+result.insertedId);
  });
 
  }, function(err) { // failureCallback
    throw (err);
  });


  
});

/* GET edit form - edit */
router.get('/:id/edit', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('editAny', 'room'), function(req, res, next) {

  db.initialize(dbName, roomCollection, function(dbCollection) { // successCallback
    var ID = req.params.id;
   // var ID= 'Kitchen';
   
   dbCollection.findOne({ _id : new ObjectId(ID)}, function(error, result) {
     if (error) throw error;
    //  console.log(result);
    
    res.render('rooms/edit', {
      title: 'Rooms - Edit',
      result: result
    });

 });
 }); 
  
});

/* POST favorite */
router.post('/:id/favorite', function(req, res, next) {
  let roomId = req.params.id;
  let favorite =  req.body.favorite;  
  db.initialize(dbName, roomCollection, function(dbCollection) {
    dbCollection.updateOne({
      _id: new ObjectId(roomId)
    },{
      $set: {
        favorite: favorite
      }
    }, function(error, result) {
      if (error) throw error;
      res.status(200).json({msg:"Favorite updated!"})
    });
});
});

/* POST edit room /:id */
router.post('/:id', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('editAny', 'room'), function(req, res, next) {

    const itemID = req.params.id;
    console.log(req.body);
    const item = req.body;
    
    console.log("Editing item: ", item);

db.initialize(dbName, roomCollection, function(dbCollection) { // successCallback
  var ID = req.params.id;

  dbCollection.updateOne({ _id : new ObjectId(itemID) },{ $set: {roomName: item.roomName, roomIcon: item.myselect} }, function (error, result) {
  if (error) throw error;

//   // send back entire updated list, to make sure frontend data is up-to-date
//   dbCollection.find().toArray(function(_error, _result) {
//     if (_error) throw _error;
// });

res.redirect('/rooms/' + ID );
  
}, function(err) { // failureCallback
throw (err);
});

}); 

});


/* POST delete room /:id/delete */
router.post('/:id/delete', userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('deleteAny', 'room'), function(req, res, next) {

  MongoClient.connect(dbConnectionUrl, function(err, db) {
    //  db.initialize(dbName, roomCollection, function(dbCollection) { // successCallback
    //   // const itemId = request.params.id;
    if (err) throw err;
     const itemID=req.params.id;
     console.log("Delete item with id: ", itemID);
     var dbo = db.db("EcoWatt");
     
     dbo.collection('Rooms').deleteOne({ _id : new ObjectId(itemID)}, function(error, result) {
      if (error) throw error;
    
      dbo.collection('Devices').deleteMany({ roomNo : new ObjectId(itemID)}, function(error, result) {
       if (error) throw error;
       
        res.status(200).json({msg:"Room deleted!"});

      });

      });

      });

    });

/* GET room details - read /:id */ 
router.get("/:id", userMiddleware.allowIfLoggedin, userMiddleware.allowIfActiveUser, userMiddleware.grantAccess('readAny', 'room'), function(req, res, next){

  db.initialize(dbName, roomCollection, function(dbCollection) { // successCallback
     var ID = req.params.id;
    // var ID= 'Kitchen';
    
    dbCollection.findOne({ _id : new ObjectId(ID)}, function(error, result) {
      if (error) throw error;
      console.log(result);

        min = Math.ceil(12);
        max = Math.floor(30);
        result.temp = Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
    
        min = Math.ceil(50);
        max = Math.floor(88);
        result.humidity =  Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
  
        min = Math.ceil(124);
        max = Math.floor(170);
        result.lightIntensity= Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
      
       
        db.initialize(dbName, deviceCollection, function(dbCollection) { // successCallback
          // const itemId = request.params.id;
         const roomID= result._id;
         console.log("Room with id: ", roomID);
    
        dbCollection.find({ roomNo : roomID}).toArray(function(_error, _result) {
            if (_error) throw _error;
            // console.log("BBBBBBBBB");
            // console.log(_result);
            result.devices = _result;
            // console.log(result.devices);
            res.render('rooms/read', {
              result: result,
            });
        });
        }, function(err) { // failureCallback
          throw (err);
        });
      
        // console.log("RRRRRRRRRRRRRRRR");
        // console.log(result.devices);
     

  });
  }); 

  }, function(err) { // failureCallback
    throw (err);
  });

module.exports = router;
